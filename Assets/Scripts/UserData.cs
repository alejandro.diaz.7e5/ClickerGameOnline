using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "User Data", menuName = "Scriptables/User Data")]
public class UserData : ScriptableObject
{
    public string Username;
    public int Score;
    public string Password;
    public bool isSuperUser;
}
