using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LogoutScript : MonoBehaviour
{
    public UserData data;
    // Start is called before the first frame update
    void LogOut()
    {
        data.Username = "";
        data.Score = 0;
        data.Password = "";
        SceneManager.LoadScene(0);
    }
  
}
