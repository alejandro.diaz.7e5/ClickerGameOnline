using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ConnectionData
{
    public static string LocalUrl = "localhost:8080";
    public static string RemoteUrl = "http://nodepotato-env.eba-vzyihngg.us-east-1.elasticbeanstalk.com:8080";
    public static bool LocalTesting = true;


    public static string GetUrl()
    {
        return LocalTesting ? LocalUrl : RemoteUrl;
    }
}
