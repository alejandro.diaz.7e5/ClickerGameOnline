using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageEffect : MonoBehaviour
{
    float transparencyDecay;
    UnityEngine.UI.Image image;
    public UnityEngine.UI.Text text;
    public UserData data;
    Coroutine routine;
    // Start is called before the first frame update
    public void Effect()
    {
        transparencyDecay = 1;
         routine = StartCoroutine(ImageAnim());
    }

    // Update is called once per frame
    IEnumerator ImageAnim()
    {
       
        image = GetComponent<UnityEngine.UI.Image>();
        image.rectTransform.localScale = Vector3.one*(1f+data.Score/100f);
        text.fontSize = 20 + (data.Score / 10);
        while (transparencyDecay >= 0)
        {
            Color color = image.color;
            transparencyDecay -= 0.1f;
            color.a = transparencyDecay;
            image.color = color;
            yield return new WaitForSeconds(0.05f);
        }
    }
}
