using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Net;
using System;
using UnityEngine.Networking;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Newtonsoft.Json;

class UpdateScript : MonoBehaviour
{
    public Text score;
    public int value;
    public UserData data;
    public Leaderboard board;
    public string baseUrl;


    private void Start()
    {
        baseUrl = ConnectionData.GetUrl();
        StartCoroutine(GetRequest());
    }
   
    IEnumerator GetRequest()
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(baseUrl + "/getscore/"))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();
            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.DataProcessingError:
                    Debug.LogError( "Error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    Debug.LogError( "HTTP Error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.Success:
                    Debug.Log( "Received: " + webRequest.downloadHandler.text);
                    break;
            }
        }
    }

    public void ButtonPress()
    {
        data.Score += 1; //Subimos al servidor el nuevo valor 
        score.text = data.Score.ToString();
        StartCoroutine(UpdateScore()); //Actualizamos el valor
    }

    IEnumerator UpdateScore()
    {
        WWWForm form = new WWWForm();
        form.AddField("score", data.Score);
        form.AddField("userName", data.Username);

        UnityWebRequest www = UnityWebRequest.Post(baseUrl + "/updatescore/", form);
            yield return www.SendWebRequest();
            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(www.error);
            }
            else
            {
            StartCoroutine(board.UpdateBoard());
                Debug.Log("Score Updated!");
            }        
    }  
}


