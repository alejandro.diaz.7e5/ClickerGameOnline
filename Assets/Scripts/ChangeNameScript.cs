using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ChangeNameScript : MonoBehaviour
{
    // Start is called before the first frame update
    private string baseUrl;
    public InputField newusername;
    public UserData userData;
    public Leaderboard board;
    void Start()
    {
        baseUrl = ConnectionData.GetUrl();
    }

    // Update is called once per frame
    void ChangeNameButton()
    {
        StartCoroutine(ChangeName());
    }
    IEnumerator ChangeName()
    {
        WWWForm form = new WWWForm();
        form.AddField("oldUserName", userData.Username);
        form.AddField("newUserName", newusername.text);

        UnityWebRequest www = UnityWebRequest.Post(baseUrl + "/updateplayers/", form);
        yield return www.SendWebRequest();
        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.Log(www.error);
        }
        else
        {
            StartCoroutine(board.UpdateBoard());
            Debug.Log("Name changed!");
        }
    }
}
