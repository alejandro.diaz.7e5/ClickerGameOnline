using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;
using UnityEngine.Networking;

public class Leaderboard : MonoBehaviour
{
    public string baseUrl;


    private void Start()
    {
        baseUrl = ConnectionData.GetUrl();
        StartCoroutine(UpdateBoard());
    }
    struct ScoreUnit
    {
        public string Username;
        public string Score;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public IEnumerator UpdateBoard()
    {
        string datadisplay = "";
        using (UnityWebRequest playerDataRemote = UnityWebRequest.Get(baseUrl + $"/getscore/"))
        {
            yield return playerDataRemote.SendWebRequest();
            ScoreUnit[] data = JsonConvert.DeserializeObject<ScoreUnit[]>(playerDataRemote.downloadHandler.text);
            foreach (ScoreUnit su in data)
            {
                datadisplay += $"{su.Username} {su.Score} \n";
            }
        }

        GetComponent<Text>().text = "Leaderboard \n" + datadisplay;
        Debug.Log("Leaderboard Updated");
    }
}
