using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class registerScript : MonoBehaviour
{

    public InputField username;
    public InputField password;
    public UserData data;
    public string baseUrl;


    private void Start()
    {
        baseUrl = ConnectionData.GetUrl();
    }
    public void RegisterButton()
    {
        string user = username.text;
        string passwd = password.text;
        //SerializeField
        StartCoroutine(Upload(user,passwd));
    }

    IEnumerator Upload(string user, string passwd)
    {
        WWWForm form = new WWWForm();
        form.AddField("score", 0);
        form.AddField("userName", user);
        form.AddField("password", passwd);
        //var asd = "'user': " + user + " 'password'" + passwd;
        //byte[] bodyRaw = Encoding.UTF8.GetBytes(asd);
        //request.uploadHandler = new UploadHandlerRaw(bodyRaw);
        //request.downloadHandler = new DownloadHandlerBuffer();
        UnityWebRequest www = UnityWebRequest.Post(baseUrl + "/setplayers/", form); //, form))
        yield return www.SendWebRequest();
            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(www.error);
            }
            else
            {
                data.Score = 0;
                data.Username = user;
                data.Password = passwd;
                Debug.Log("Register complete!");
                UnityEngine.SceneManagement.SceneManager.LoadScene(0);
            }       
    }
}

