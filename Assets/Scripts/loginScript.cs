using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Newtonsoft.Json;

public class loginScript : MonoBehaviour
{
    struct ConvertedData
    {
        public string Username;
        public int Score;
        public string Password;
    }
    public InputField username;
    public InputField password;
    public UserData data;
    public string baseUrl;

    private void Start()
    {
        baseUrl = ConnectionData.GetUrl();
    }
    public void LoginButton()
    {
        string user = username.text;
        string passwd = password.text;
        //SerializeField
        StartCoroutine(Upload(user, passwd));
    }

    IEnumerator Upload(string user, string passwd)
    {
        //var asd = "'user': " + user + " 'password'" + passwd;
        //byte[] bodyRaw = Encoding.UTF8.GetBytes(asd);
        //request.uploadHandler = new UploadHandlerRaw(bodyRaw);
        //request.downloadHandler = new DownloadHandlerBuffer();
        UnityWebRequest www = UnityWebRequest.Get(baseUrl + $"/loginplayers/{user}&{passwd}");

        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.Log(www.error);
        }
        else
        {

            using (UnityWebRequest playerDataRemote = UnityWebRequest.Get(baseUrl + $"/getplayers/{user}"))
            {
                yield return playerDataRemote.SendWebRequest();
                ConvertedData rawConvert = JsonConvert.DeserializeObject<ConvertedData>(playerDataRemote.downloadHandler.text);
                data.Score = rawConvert.Score;
                data.Password = rawConvert.Password;
                data.Username = rawConvert.Username;
            }
            Debug.Log("Login complete!");
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
    }
}

